## RESULTS OF THE ANALYSIS

You can check the results of 4 different map/reduce queries across the whole dataset of US flights from 1994 to 2008 in the folder "results":

1. output1: Amount of cancelled flights per year
2. output2: Amount of delayed flights per year
3. output3: Amount of flights that were able to halve their departure delays
3. output4: Negative scores based on number of delayed flights per airports (both absolute and relative scores)


## HOW TO DEPLOY HDFS

To start the HDFS go in hadoop installation home and enter:

sbin/start-dfs.sh
sbin/start-yarn.sh

HDFS dashboard on: localhost:9870
YARN dashboard on: localhost:8088

to disable safemode:
hdfs dfsadmin -safemode leave
