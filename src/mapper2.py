#!/usr/bin/env python

import sys


for row in sys.stdin:

    # Split
    row = row.strip().split(',')

    # Map
    if row[0] != 'Year':
        year = int(row[0])
        month = int(row[1])
        day = int(row[2])
        wday = int(row[3])

        # Leap year
        if year % 4 == 0:
            month_days = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        else:
            month_days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

        # Find week
        i = 1
        days_months_before = 0
        while month > i:
            days_months_before = days_months_before + month_days[i]
            i += 1
        monday_before = days_months_before + day - (wday - 1)
        if monday_before > 0:
            week = int(monday_before / 7)
        elif monday_before == 0 or (monday_before == -1 and ((year - 1) % 4 == 0)):
            year = year - 1
            month = 12
            week = 53
        else:
            year = year - 1
            month = 12
            week = 52

        year = str(year)
        week = str(week).zfill(2)

        # Output
        wdelay = row[25]
        delays = row[24:]
        if wdelay == 'NA':
            print "{}\t{}".format(year+week, -1)
        else:
            delays = map(int, delays)
            if sum(delays) == 0:
                print "{}\t{}".format(year+week, -1)
            else:
                print "{}\t{}".format(year+week, wdelay)
