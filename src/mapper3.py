#!/usr/bin/env python

import sys

for row in sys.stdin:
	
	# Split
	row = row.strip().split(',')
	
	# Map
	if row[0] != 'Year':
		arrDelay = row[14]
		depDelay = row[15]
		distance = row[18]
		
		#Division by distance groups
		if distance != 'NA':
			distance = int(distance)
		dgroup = '14'
		if distance < 200:
			dgroup = '01'
		elif 200 <= distance and distance < 400:
			dgroup = '02'
		elif 400 <= distance and distance < 600:
			dgroup = '03'
		elif 600 <= distance and distance < 800:
			dgroup = '04'
		elif 800 <= distance and distance < 1000:
			dgroup = '05'
		elif 1000 <= distance and distance < 1200:
			dgroup = '06'
		elif 1200 <= distance and distance < 1400:
			dgroup = '07'
		elif 1400 <= distance and distance < 1600:
			dgroup = '08'
		elif 1600 <= distance and distance < 1800:
			dgroup = '09'
		elif 1800 <= distance and distance < 2000:
			dgroup = '10'
		elif 2000 <= distance and distance < 2200:
			dgroup = '11'
		elif 2200 <= distance and distance < 2400:
			dgroup = '12'
		elif 2400 <= distance and distance < 2600:
			dgroup = '13'

		#Does it have halved its delay
		if depDelay == 'NA':
			depDelay = '0'
		if arrDelay == 'NA':
			arrDelay = '0'
		depDelay = float(depDelay)
		arrDelay = float(arrDelay)
		halved = 0
		if depDelay > 0:
			if arrDelay <= depDelay/2:
				halved = 1
				print "{}\t{}".format(dgroup, halved)
			else:
				print "{}\t{}".format(dgroup, halved)
