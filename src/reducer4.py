#!/usr/bin/env python

import sys

finalScore = 0.0
total = 0.0
oldAirport = None

for line in sys.stdin:
	data = line.strip().split("\t")
	if len(data) != 2:
		# Something has gone wrong. Skip this line.
		continue

	newAirport, score = data
	if oldAirport and oldAirport != newAirport:
		print "{}\t{}\t{}\t{}".format(oldAirport[:3], oldAirport[3:], finalScore, (finalScore/total)*100)
		finalScore = 0.0
		total = 0.0

	oldAirport = newAirport
	total += 1.0
	finalScore += float(score)

if oldAirport:
	print "{}\t{}\t{}\t{}".format(oldAirport[:3], oldAirport[3:], finalScore, (finalScore/total)*100)
	finalScore = 0.0
	total = 0.0
