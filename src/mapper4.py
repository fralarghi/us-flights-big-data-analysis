#!/usr/bin/env python

import sys

for row in sys.stdin:
	
	# Split
	row = row.strip().split(',')
	
	# Map
	if row[0] != 'Year':
		year = int(row[0])
		month = int(row[1])
		day = int(row[2])
		wday = int(row[3])
		arrDelay = row[14]
		depDelay = row[15]
		origAirport = row[16]
		destAirport = row[17]

		# Leap year
		if year % 4 == 0:
			month_days = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
		else:
			month_days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

		# Find week
		i = 1
		days_months_before = 0
		while month > i:
			days_months_before = days_months_before + month_days[i]
			i += 1
		monday_before = days_months_before + day - (wday - 1)
		if monday_before > 0:
			week = int(monday_before / 7)
		elif monday_before == 0 or (monday_before == -1 and ((year - 1) % 4 == 0)):
			year = year - 1
			month = 12
			week = 53
		else:
			year = year - 1
			month = 12
			week = 52

		year = str(year)
		week = str(week).zfill(2)
		
		# Assign scores
		origScore = 0.0
		destScore = 0.0
		if depDelay == 'NA':
			depDelay = '0'
		if arrDelay == 'NA':
			arrDelay = '0'
		if int(depDelay) > 15:
			origScore = 1.0
		if int(arrDelay) > 15:
			destScore = 0.5

		print "{}\t{}".format(origAirport+year+week, origScore)
		print "{}\t{}".format(destAirport+year+week, destScore)
