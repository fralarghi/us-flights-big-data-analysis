#!/usr/bin/env python

import sys

total = 0
delayed = 0
weatherDelayed = 0
gtotal = 0
gdelayed = 0
gweatherDelayed = 0
oldWeek = None

for line in sys.stdin:
	data = line.strip().split("\t")
	if len(data) != 2:
		# Something has gone wrong. Skip this line.
		continue

	newWeek, weatherDelay = data

	if oldWeek and oldWeek != newWeek and total != 0:
		if delayed == 0:
			print "{}\t{}\t{}".format(oldWeek, 0, 0)
		else:
			print "{}\t{}\t{}".format(oldWeek, float(float(delayed)/float(total))*100, float(float(weatherDelayed)/float(delayed)) * 100)

		oldWeek = newWeek
		gtotal += total
		gdelayed += delayed
		gweatherDelayed += weatherDelayed
		total = 0
		delayed = 0
		weatherDelayed = 0

	oldWeek = newWeek
	total += 1
	weatherDelay = int(weatherDelay)
	if weatherDelay != -1:
		delayed += 1
	if weatherDelay > 0:
		weatherDelayed += 1

if oldWeek:
	if delayed == 0:
		print "{}\t{}\t{}".format(oldWeek, 0, 0)
	else:
		print "{}\t{}\t{}".format(oldWeek, float(float(delayed)/float(total))*100, float(float(weatherDelayed)/float(delayed)) * 100)
