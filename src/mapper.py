#!/usr/bin/env python

import sys

for row in sys.stdin:
	
	# Split
	row = row.strip().split(',')
	
	# Map
	if row[0] != 'Year':
		year = row[0]
		month = row[1].zfill(2)
		day = row[2].zfill(2)
		cancelled = row[21]
		print "{0}\t{1}".format(year+month+day, cancelled)
