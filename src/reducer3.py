#!/usr/bin/env python

import sys

total = 0.0
totalHalved = 0.0

oldGroup = None

for line in sys.stdin:
	data = line.strip().split("\t")
	if len(data) != 2:
		# Something has gone wrong. Skip this line.
		continue

	newGroup, halved = data
	if oldGroup and oldGroup != newGroup:
		print "{}\t{}".format(oldGroup, (totalHalved/total)*100)
		total = 0.0
		totalHalved = 0.0
		
	oldGroup = newGroup
	total += 1
	if int(halved) == 1:
		totalHalved += 1

if oldGroup != None:
	print "{}\t{}".format(oldGroup, (totalHalved/total)*100)
