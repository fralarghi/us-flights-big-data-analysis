#!/usr/bin/env python

import sys

flights = 0
totalFlights = 0
cancelledFlights = 0
totalCancelled = 0
oldDay = None

for line in sys.stdin:
	data = line.strip().split("\t")
	if len(data) != 2:
		# Something has gone wrong. Skip this line.
		continue

	newDay, thisCancelled = data
	if oldDay and oldDay != newDay:
		if flights != 0:
			print oldDay, "\t", float(float(cancelledFlights)/float(flights))*100
		oldDay = newDay
		flights = 0
		cancelledFlights = 0

	oldDay = newDay
	flights += 1
	totalFlights += 1
	if thisCancelled == '1':
		cancelledFlights += 1
		totalCancelled += 1

if oldDay != None and flights != 0:
	print oldDay, "\t", float(float(cancelledFlights)/float(flights))*100
