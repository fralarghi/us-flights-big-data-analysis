#!/usr/bin/env python

import matplotlib.pyplot as plt
import datetime as dt
import sys

def printPlot(weeks, percsLate, percsWeatherLate):
    plt.style.use('ggplot')
    plt.plot(weeks, percsLate, label='% of delayed flights on total')
    plt.plot(weeks, percsWeatherLate, label='% of delayed flights due to weather on total delayed')
    plt.legend(loc="upper right")
    plt.xlabel('Weeks')
    plt.ylabel('Percentage')
    plt.title('Amount of delayed flights in '+oldYear)
    plt.yticks([-5,0,10,20,30,40,50,60,70,80,90,100,105],['-','0%','10%','20%','30%','40%','50%','60%','70%','80%','90%','100%','-'])
    plt.xticks([])
    plt.grid(True)
    plt.savefig("plot"+oldYear+".png")
    plt.clf()

weeks = []
percsLate = []
percsWeatherLate = []
oldYear="2004"

for line in sys.stdin:
    line = line.split("\t")
    week, percLate, percWeatherLate = line
    year = week[:4]

    if year != oldYear:

        printPlot(weeks, percsLate, percsWeatherLate)

        del weeks[:]
        del percsLate[:]
        del percsWeatherLate[:]
    
    oldYear = year
    weeks.append(week[4:])
    percsLate.append(round(float(percLate),2))
    percsWeatherLate.append(round(float(percWeatherLate),2))

printPlot(weeks, percsLate, percsWeatherLate)
