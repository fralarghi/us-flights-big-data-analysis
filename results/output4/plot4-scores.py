#!/usr/bin/env python

import matplotlib.pyplot as plt
import datetime as dt
import sys

def printAirports():
    plt.xlabel('Weeks')
    plt.ylabel('Scores')
    plt.title('Negative scores')
    #plt.yticks([-5,0,10,20,30,40,50,60,70,80,90,100,105],['-','0%','10%','20%','30%','40%','50%','60%','70%','80%','90%','100%','-'])
    plt.grid(True)
    plt.savefig("scores/plot"+str(i)+".png")
    plt.clf()

weeks = []
scores = []
percs = []
counter = 1
i = 1

oldAirport="ABE"
plt.style.use('ggplot')

for line in sys.stdin:
    line = line.split("\t")
    airport, week, score, perc = line

    if airport != oldAirport:
        counter += 1
        plt.scatter(weeks, scores, label=oldAirport)
        #plt.scatter(weeks, percs, label=oldAirport)
        plt.legend(loc="upper right")
        del weeks[:]
        del scores[:]
        del percs[:]

        if counter > 7:
            printAirports()
            counter = 1
            i += 1
    
    oldAirport = airport
    weeks.append(dt.datetime.strptime(week,'%Y%m%d').date())
    scores.append(round(float(score),1))
    percs.append(round(float(perc),2))

printAirports()