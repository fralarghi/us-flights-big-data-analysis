#!/usr/bin/env python

import matplotlib.pyplot as plt
import datetime as dt
import sys

def printPlot(days, percs):
    plt.style.use('ggplot')
    plt.scatter(days, percs)
    plt.xlabel('Days')
    plt.ylabel('Percentage')
    plt.title('Amount of cancelled flights in '+oldYear)
    plt.yticks([-5,0,10,20,30,40,50,60,70,80,90,100,105],['-','0%','10%','20%','30%','40%','50%','60%','70%','80%','90%','100%','-'])
    plt.grid(True)
    plt.savefig("plot"+oldYear+".png")
    plt.clf()

days = []
percs = []
oldYear="1994"

for line in sys.stdin:
    line = line.split("\t")
    day, perc = line
    year = day[:4]

    if year != oldYear:

        printPlot(days, percs)

        del days[:]
        del percs[:]
        
    oldYear = year
    days.append(dt.datetime.strptime(day,'%Y%m%d ').date())
    percs.append(round(float(perc),2))

printPlot(days, percs)