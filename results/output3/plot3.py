#!/usr/bin/env python

import matplotlib.pyplot as plt
import sys

def printPlot(groups, percs):
    plt.style.use('ggplot')
    plt.scatter(groups, percs)
    plt.xlabel('Distance groups: 1 = 0-199 mi, 2 = 200-399 mi , ... , 14 = >2600 mi')
    plt.ylabel('Percentage')
    plt.title('Amount of flights that were able\nto halve their departure delays')
    plt.yticks([-5,0,10,20,30,40,50,60,70,80,90,100,105],['-','0%','10%','20%','30%','40%','50%','60%','70%','80%','90%','100%','-'])
    plt.xticks([1,2,3,4,5,6,7,8,9,10,11,12,13,14])
    plt.grid(True)
    plt.savefig("plot.png")
    plt.clf()

groups = []
percs = []

for line in sys.stdin:
    line = line.split("\t")
    group, perc = line
        
    groups.append(int(group))
    percs.append(round(float(perc),2))

printPlot(groups, percs)